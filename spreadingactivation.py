"""
spreadingactivation.py

SpreadingActivation
AI text generating software

Based on the Breadth-first search algorithm.
A modified spreading activation model is used.
Many parameters and functionalities are made to mimic the human brain, such as chunking, excitation/inhibition and memory decay.


ArsCyber™ Software Trademark
© Copyright
All rights reserved.

This software is in its early alpha stage.
"""

#To do: Unit test

import random
import os
import re

 
#The settings
RANDOM_SEARCH = False #Add randomness to the search of the graph: True / False
RANDOM_SEARCH_RANDOMNESS = 8 #The level of randomness added to the vertex search.
SEARCH_REINITIALIZATION = True #Reset the visited Breadth-first vertices list with a given frequency: True / False
REINITIALIZATION_FREQUENCY = 4 #The rate of memory trace decay. Frequency of Breadth-first visited list reset. Bigger value implies the AI performs less resets and has a better working memory, and as a consequence has less repetitions and a shorter output.
MAXIMUM_SEARCH_ITERATIONS = 200 #Maximum iterations of the Breadth-first search. Higher value tends to lead to longer outputs.
CHUNK_SIZE = 1 #Split the text to N chunks. Larger chunking leads to more coherent but less creative text, as well as longer outputs.
firstElement = "Alice" #Element to start the generated text with.
TEXT_DATASET_LOCATION = "text.txt" #Text dataset location
WORKING_MEMORY_SIZE = 20 #The size of working memory.


#Lists and dictionaries for the Breadth-first algorithm
visited = []
workingMemory = set()
queue = []
n_chunk = []
graph = {}
weight = {}

#Load the first element into buffers
lastNode = firstElement
output = firstElement
queue.append(firstElement)
visited.append(firstElement)


#Parse text
text = ""
f = open(TEXT_DATASET_LOCATION, encoding="utf8")
text = f.read()
f.close()


text = text.replace("\r", "")
text = re.sub(r'\n+', ' ', text)


def splitTheText():

    global splitText
    global n_chunk
    global CHUNK_SIZE
    
    splitText = text.split(" ")
    

    for i in range(0, len(splitText), CHUNK_SIZE):
        n_chunk.append(" ".join(splitText[i:i+CHUNK_SIZE]))



def buildGraph():

    global graph
    global n_chunk
    global splitText
    
    #Load the graph
    for i in range(0, len(n_chunk)):

        if i < (len(n_chunk)-1):
        
            if n_chunk.count(splitText[i]) == 1:
                graph[n_chunk[i]] = [n_chunk[i+1]]
                pass
            else:
                if splitText[i] in graph:
                    graph[n_chunk[i]].append(n_chunk[i+1])
                else:
                    graph[n_chunk[i]] = [n_chunk[i+1]]

        else:
            graph[n_chunk[i]] = []


def loadWeights():
    global graph
    global weight
    
    #Load graph weights
    for v in graph:

        for v2 in graph[v]:
            weight[tuple([v, v2])] = graph[v].count(v2)
            
        graph[v] = set(graph[v])


#Check if two vertices are adjacent under a given graph
def isAdjacent(graphObj, v1, v2):
    for v in graphObj:
        if (v == v1) and (v2 in graphObj[v1]):
            return True
            break

    return False


#Check if a set of vertices are connected under a given graph
def areConnected(graphObj, vertices):
    for i in range(0, len(vertices)):
        if i < len(vertices)-1:
            if isAdjacent(graphObj, vertices[i], vertices[i+1]) == False:
                return False
                break

    return True        


def firstElementSearch():
    global queue
    global output
    global n_chunk
    global firstElement
    global visited
    global lastNode
    
    #Find the first element
    for v in n_chunk:
        if firstElement in v:
            firstElement = v
            lastNode = firstElement
            output = firstElement
            queue = []
            visited = []
            queue.append(firstElement)
            visited.append(firstElement)
            break


#Greatest weight between a vertex and a set of vertices
def greatestWeight(vertex, vertices):

    #Inhibition and excitation are handled here.
    #The weights can be adjusted depending on the needed activation patterns.
    #For example, if a specific emotional overtone needs to be selected, the weights can be adjusted accordingly.

    global weight
    global RANDOM_SEARCH
    global RANDOM_SEARCH_RANDOMNESS
    
    weight1 = {}
    
    for v in vertices:
        if (vertex, v) in weight:
            weight1[(vertex, v)] = weight[(vertex, v)]


    if len(weight1) > 0:
        
        maxValue = max(weight1.values())
        
        if RANDOM_SEARCH == True:
            maxRandomness = len(weight1)
            if (len(weight1) > RANDOM_SEARCH_RANDOMNESS):
                maxRandomness = RANDOM_SEARCH_RANDOMNESS
                
            maxValue = list(sorted(weight1.values()))[-(random.randint(1, maxRandomness))]
            

        for e in weight1:
            if weight1[e] == maxValue:
                return e
                break
    else:
        return False


i = 0

def spreadingActivation():

    global i
    global workingMemory
    global visited
    global lastNode
    global output
    global queue
    global graph
    
    #The Breadth-first spreading activation algorithm loop
    while((len(queue) != 0) and (i < MAXIMUM_SEARCH_ITERATIONS)):
        
        i = i + 1
        
        nextNode = queue.pop(0)

        if SEARCH_REINITIALIZATION == True:
            if i%REINITIALIZATION_FREQUENCY == 0:
                visited = []
        
        for v in graph:
            if isAdjacent(graph, nextNode, v):
                if (v not in visited):
                    queue.append(v)
                    visited.append(v)
                    workingMemory.add(v)
                    
                if len(workingMemory) >= WORKING_MEMORY_SIZE:
                
                    if greatestWeight(lastNode, workingMemory) is not False:
                        if isAdjacent(graph, lastNode, greatestWeight(lastNode, workingMemory)[1]):
                            output = output + " " + greatestWeight(lastNode, workingMemory)[1]
                            lastNode = greatestWeight(lastNode, workingMemory)[1]
            
                    workingMemory = set()

        print("Thought " + str(i) + " out of " + str(MAXIMUM_SEARCH_ITERATIONS))


def generateText():

    global graph
    global n_chunk

    graph = {}
    n_chunk = []
    
    print("Splitting text...")
    splitTheText()

    print("Building the graph...")
    buildGraph()

    print("Loading the weights...")
    loadWeights()
        
    print("Searching for the first element...")
    firstElementSearch()

    print("Generating text...")
    spreadingActivation()
    


def main():

    generateText()
    print("Generated text: " + output)


if __name__ == "__main__":
    main()
